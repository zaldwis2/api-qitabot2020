<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Checkins;
use App\Models\Restaurants;
use App\Models\MenuCategories;
use App\Models\Menus;
use App\Models\Tables;
use App\Models\Orders;
use App\Models\ItemOrders;
use App\Models\Callings;
use App\Models\FeedbackAnswers;
use App\Models\Feedbacks;
use App\Models\FeedbackOptionals;
use App\Models\Admins;
use App\Models\RestaurantAdmins;
use App\Models\RestaurantOfficers;
use App\Models\Users;
use App\Models\Deals;
use App\Models\OpeningHours;
use DateTime;
use App\Services\NotificationService;
use LaravelFCM\Message\Topics;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class CMSController extends Controller
{
    protected $image_deals_folder = '/public/deals_picture/';
    protected $image_menu_folder = '/public/menus_picture/';
    protected $image_profile_folder = '/public/profile_picture/';
    protected $image_restaurant_folder = '/public/restaurant_picture/';


    public function __construct(){
        $this->middleware('auth');
    }

    //START
    //Method for Super Admin
    public function RegisterSuperAdmin(Request $request){
        try {

            $admin=Admins::where('email','=',$request->email)->first();
            if(!$admin){
                $user = new Admins();
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->name = $request->name;
                $user->gender = $request->gender;
                $user->birth_date = $request->birth_date;
                if ($request->hasFile('image')) {
                    if ($request->file('image')->isValid()) {
                        $file_ext        = $request->file('image')->getClientOriginalExtension();
                        $file_size       = $request->file('image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_profile_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $profile_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image')->move($dest_path, $profile_image);
                            $user->image= $profile_image;
                        }
                    }
                }    
                $user->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil mendaftar',
                    'dataAdmin' => [$user]
                ];    
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Email Sudah Terdaftar',
                ]; 
            }
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function AddRestaurant(Request $request){
        try {
            $admin=RestaurantAdmins::where('email','=',$request->email)->first();
            if(!$admin){
                $user = new Restaurants();
                $user->name = $request->restaurant_name;
                $user->description = $request->description;
                $user->address = $request->address;
                $user->phone = $request->phone;
                $user->is_chat_enable = '1';
                if ($request->hasFile('restaurant_image')) {
                    if ($request->file('restaurant_image')->isValid()) {
                        $file_ext        = $request->file('restaurant_image')->getClientOriginalExtension();
                        $file_size       = $request->file('restaurant_image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_restaurant_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('restaurant_image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $restaurant_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('restaurant_image')->move($dest_path, $restaurant_image);
                            $user->image= $restaurant_image;
                        }
                    }
                }    
                $user->saveOrFail();
    
                $admin = new RestaurantAdmins();
                $admin->email = $request->email;
                $admin->password = Hash::make($request->password);
                $admin->name = $request->name;
                $admin->gender = $request->gender;
                $admin->birth_date = $request->birth_date;
                $admin->id_restaurant = $user->id;
                if ($request->hasFile('image')) {
                    if ($request->file('image')->isValid()) {
                        $file_ext        = $request->file('image')->getClientOriginalExtension();
                        $file_size       = $request->file('image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_profile_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $profile_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image')->move($dest_path, $profile_image);
                            $admin->image= $profile_image;
                        }
                    }
                }    
                $admin->saveOrFail();
    
    
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil mendaftar',
                    'dataAdminRestaurant' => [$admin]
                ];    
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Email Sudah Terdaftar',
                ]; 
            }
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function loginAdmin(Request $request){
        $password = $request->password;
        try{
            $Admin=Admins::where('email','=',$request->email)->first();

            if(!$Admin){
                $statusCode = 404;                    
                $response = [
                ];       
            } else {
                $dataPassword = $Admin->password;
                if(Hash::check($password, $dataPassword)){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Login Berhasil',
                        'dataAdmin' => [$Admin]
                    ];    
                } else {
                    $statusCode = 404;                    
                    $response = [
                        'error' => true,
                        'message' => 'Password atau Email Salah'
                    ];       
                }
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Login Gagal';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllDeals(Request $request){
        try {
            $dataDeals = DB::table('deals')
            ->join('restaurants', 'deals.id_restaurant', '=', 'restaurants.id')
            ->select('deals.*','restaurants.name')
            ->orderBy('deals.created_at', 'ASC')
            ->get();
            if(!$dataDeals->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataDeals' => $dataDeals
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function AddDeals(Request $request){
        try {
            $deals = new Deals();
            $deals->id_restaurant = $request->id_restaurant;
            $deals->title = $request->title;
            $deals->description = $request->description;
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $file_ext        = $request->file('image')->getClientOriginalExtension();
                    $file_size       = $request->file('image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = base_path() . $this->image_deals_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $deal_image = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('image')->move($dest_path, $deal_image);
                        $deals->image= $deal_image;
                    }
                }
            }    
            $deals->saveOrFail();    
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Ditambah',
                'dataDeals' => [$deals]
            ];    
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Ditambah',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateDeals(Request $request){
        try{
            $deals=Deals::where('id','=',$request->id_deal)->first();

            if($request->is_active != ""){
                $deals->is_active = $request->is_active;
            }
            if($request->title != ""){
                $deals->title = $request->title;
            }
            if($request->description != ""){
                $deals->description = $request->description;
            }
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $file_ext        = $request->file('image')->getClientOriginalExtension();
                    $file_size       = $request->file('image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = base_path() . $this->image_deals_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $deal_image = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('image')->move($dest_path, $deal_image);
                        $deals->image= $deal_image;
                    }
                }
            }    
            $deals->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];   
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllRestaurantAdmin(Request $request){
        try {
            $data = DB::table('restaurant_admins')
            ->join('restaurants', 'restaurant_admins.id_restaurant', '=', 'restaurants.id')
            ->select('restaurant_admins.*','restaurants.name')
            ->orderBy('restaurant_admins.created_at', 'ASC')
            ->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataAdmins' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateRestaurantAdmin(Request $request){
        try{
            $restaurantAdmin=RestaurantAdmins::where('id','=',$request->id_restaurant_admin)->first();
            $query=RestaurantAdmins::where('email','=',$request->email)->first();

            if($query){
                $statusCode = 404;                    
                $response = [
                    'error' => true,
                    'message' => 'Email Already Exist',
                ];      
            }else{
                if($request->email != ""){
                    $restaurantAdmin->email = $request->email; 
                }
                if($request->password != ""){
                    $restaurantAdmin->password = Hash::make($request->password);
                }
                if($request->name != ""){
                    $restaurantAdmin->name = $request->name;
                }
                if($request->gender != ""){
                    $restaurantAdmin->gender = $request->gender;
                }
                if($request->birth_date != ""){
                    $restaurantAdmin->birth_date = $request->birth_date;
                }
                if($request->id_restaurant != ""){
                    $restaurantAdmin->id_restaurant = $request->id_restaurant;
                }
                if ($request->hasFile('image')) {
                    if ($request->file('image')->isValid()) {
                        $file_ext        = $request->file('image')->getClientOriginalExtension();
                        $file_size       = $request->file('image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_profile_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $profile_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image')->move($dest_path, $deal_image);
                            $restaurantAdmin->image= $profile_image;
                        }
                    }
                }    
                $restaurantAdmin->saveOrFail();
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'update berhasil',
                ];     
            }
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getRestaurantStatistic(Request $request){
        try {
            $usersPerMonth =Restaurants::select(DB::raw('count(id) as `data`'),DB::raw("DATE_FORMAT(created_at, '%Y-%m') date"))
            ->groupBy('date')->orderBy('date')->get();
            if(!$usersPerMonth->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'restaurantStatistics' => $usersPerMonth
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getUsersStatistic(Request $request){
        try {
            $usersPerMonth =Users::select(DB::raw('count(id) as `data`'),DB::raw("DATE_FORMAT(created_at, '%Y-%m') date"))
            ->groupBy('date')->orderBy('date')->get();
            if(!$usersPerMonth->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'usersStatistic' => $usersPerMonth
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    //Method for Super Admin
    //END

    //START
    //Method for Restaurant Admin

    public function loginRestaurantAdmin(Request $request){
        $password = $request->password;
        try{
            $Admin=RestaurantAdmins::where('email','=',$request->email)->first();

            if(!$Admin){
                $statusCode = 404;                    
                $response = [
                ];       
            } else {
                $dataPassword = $Admin->password;
                if(Hash::check($password, $dataPassword)){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Login Berhasil',
                        'dataAdminRestaurant' => [$Admin]
                    ];    
                } else {
                    $statusCode = 404;                    
                    $response = [
                        'error' => true,
                        'message' => 'Password atau Email Salah'
                    ];       
                }
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Login Gagal';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }   

    public function registerRestaurantOfficers(Request $request){
        try {

            $admin=RestaurantOfficers::where('email','=',$request->email)->first();
            if(!$admin){
                $user = new RestaurantOfficers();
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->name = $request->name;
                $user->gender = $request->gender;
                $user->birth_date = $request->birth_date;
                $user->id_restaurant = $request->id_restaurant;
                if ($request->hasFile('image')) {
                    if ($request->file('image')->isValid()) {
                        $file_ext        = $request->file('image')->getClientOriginalExtension();
                        $file_size       = $request->file('image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_profile_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $profile_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image')->move($dest_path, $profile_image);
                            $user->image= $profile_image;
                        }
                    }
                }    
                $user->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil mendaftar',
                    'dataAdmin' => [$user]
                ];    
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Email Sudah Terdaftar',
                ]; 
            }
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllRestaurantOfficer(Request $request){
        try {
            $data = DB::table('restaurant_officers')
            ->join('restaurants', 'restaurant_officers.id_restaurant', '=', 'restaurants.id')
            ->select('restaurant_officers.*','restaurants.name')
            ->orderBy('restaurant_officers.created_at', 'ASC')
            ->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataRestaurantOfficers' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateRestaurantOfficers(Request $request){
        try{
            $restaurantOfficer=RestaurantOfficers::where('id','=',$request->id_restaurant_officer)->first();
            $query=RestaurantOfficers::where('email','=',$request->email)->first();

            if($query){
                $statusCode = 404;                    
                $response = [
                    'error' => true,
                    'message' => 'Email Already Exist',
                ];      
            }else{
                if($request->email != ""){
                    $restaurantOfficer->email = $request->email; 
                }
                if($request->password != ""){
                    $restaurantOfficer->password = Hash::make($request->password);
                }
                if($request->name != ""){
                    $restaurantOfficer->name = $request->name;
                }
                if($request->gender != ""){
                    $restaurantOfficer->gender = $request->gender;
                }
                if($request->birth_date != ""){
                    $restaurantOfficer->birth_date = $request->birth_date;
                }
                if ($request->hasFile('image')) {
                    if ($request->file('image')->isValid()) {
                        $file_ext        = $request->file('image')->getClientOriginalExtension();
                        $file_size       = $request->file('image')->getClientSize();
                        $allow_file_exts = array('jpeg', 'jpg', 'png');
                        $max_file_size   = 1024 * 1024 * 10;
                        if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                            $dest_path     = base_path() . $this->image_profile_folder;
                            $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                            $file_name     = str_replace(' ', '-', $file_name);
                            $profile_image = $file_name  . '.' . $file_ext;
        
                            // move file to serve directory
                            $request->file('image')->move($dest_path, $deal_image);
                            $restaurantOfficer->image= $profile_image;
                        }
                    }
                }    
                $restaurantOfficer->saveOrFail();
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'update berhasil',
                ];     
            }
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateRestaurant(Request $request){
        try{
            $restaurant=Restaurants::where('id','=',$request->id_restaurant)->first();
            if($request->name != ""){
                $restaurant->name = $request->name; 
            }
            if($request->description != ""){
                $restaurant->description = $request->description; 
            }
            if($request->address != ""){
                $restaurant->address = $request->address; 
            }
            if($request->phone != ""){
                $restaurant->phone = $request->phone; 
            }
            if($request->max_response_time != ""){
                $restaurant->max_response_time = $request->max_response_time; 
            }
            if($request->is_chat_enable != ""){
                $restaurant->is_chat_enable = $request->is_chat_enable; 
            }
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $file_ext        = $request->file('image')->getClientOriginalExtension();
                    $file_size       = $request->file('image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = base_path() . $this->image_restaurant_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $restaurant_image = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('image')->move($dest_path, $restaurant_image);
                        $restaurant->image= $restaurant_image;
                    }
                }
            }    
            $restaurant->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function AddMenu(Request $request){
        try {
            $menu = new Menus();
            $menu->id_restaurant = $request->id_restaurant;
            $menu->id_menu_category = $request->id_menu_category;
            $menu->name = $request->name;
            $menu->description = $request->description;
            $menu->price = $request->price;
            $menu->is_available = '1';
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $file_ext        = $request->file('image')->getClientOriginalExtension();
                    $file_size       = $request->file('image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = base_path() . $this->image_menu_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $menu_image = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('image')->move($dest_path, $menu_image);
                        $menu->image= $menu_image;
                    }
                }
            }    
            $menu->saveOrFail();    
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Ditambah',
                'dataMenu' => [$menu]
            ];    
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Ditambah',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllMenus(Request $request){
        try {
            $data = DB::table('menus')
            ->join('menu_categories', 'menus.id_menu_category', '=', 'menu_categories.id')
            ->select('menus.*','menu_categories.name')
            ->where('menus.id_restaurant', $request->id_restaurant)
            ->orderBy('menus.created_at', 'ASC')
            ->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataAllMenu' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateMenu(Request $request){
        try{
            $menu=Menus::where('id','=',$request->id_menu)->first();
            if($request->id_menu_category != ""){
                $menu->id_menu_category = $request->id_menu_category; 
            }
            if($request->name != ""){
                $menu->name = $request->name; 
            }
            if($request->description != ""){
                $menu->description = $request->description; 
            }
            if($request->price != ""){
                $menu->price = $request->price; 
            }
            if($request->is_available != ""){
                $menu->is_available = $request->is_available; 
            }
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $file_ext        = $request->file('image')->getClientOriginalExtension();
                    $file_size       = $request->file('image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = base_path() . $this->image_menu_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $menu_image = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('image')->move($dest_path, $menu_image);
                        $menu->image= $menu_image;
                    }
                }
            }    
            $menu->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function AddMenuCategory(Request $request){
        try {
            $category = new MenuCategories();
            $category->id_restaurant = $request->id_restaurant;
            $category->name = $request->name;
            $category->saveOrFail();    
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Ditambah',
                'dataCategory' => [$category]
            ];    
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Ditambah',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllCategory(Request $request){
        try {
            $data = DB::table('menu_categories')
            ->select('menu_categories.*')
            ->where('menu_categories.id_restaurant', $request->id_restaurant)
            ->orderBy('menu_categories.created_at', 'ASC')
            ->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataCategories' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateCategory(Request $request){
        try{
            $category=MenuCategories::where('id','=',$request->id_category)->first();
            $category->name = $request->name; 
            $category->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function AddOpenHour(Request $request){
        try {
            $data=OpeningHours::where('id_restaurant','=',$request->id_restaurant)->first();
            if($data){
                $statusCode = 404;                    
                $response = [
                    'error' => true,
                    'message' => 'Data Exist',
                ];    
            }else{
                $hour = new OpeningHours();
                $hour->id_restaurant = $request->id_restaurant;
                $hour->Monday = $request->Monday;
                $hour->Tuesday = $request->Tuesday;
                $hour->Wednesday = $request->Wednesday;
                $hour->Thursday = $request->Thursday;
                $hour->Friday = $request->Friday;
                $hour->Saturday = $request->Saturday;
                $hour->Sunday = $request->Sunday;
                $hour->saveOrFail();    
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Ditambah',
                    'dataHours' => [$hour]
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Ditambah',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getRestaurantOpenHour(Request $request){
        try {
            $data = DB::table('opening_hours')
            ->select('opening_hours.*')
            ->where('opening_hours.id_restaurant', $request->id_restaurant)
            ->first();
            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataOpenHour' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateRestaurantOpenHour(Request $request){
        try{
            $hour=OpeningHours::where('id_restaurant','=',$request->id_restaurant)->first();
            if($request->Monday != ""){
                $hour->Monday = $request->Monday; 
            }
            if($request->Tuesday != ""){
                $hour->Tuesday = $request->Tuesday; 
            }
            if($request->Wednesday != ""){
                $hour->Wednesday = $request->Wednesday; 
            }
            if($request->Thursday != ""){
                $hour->Thursday = $request->Thursday; 
            }
            if($request->Friday != ""){
                $hour->Friday = $request->Friday; 
            }
            if($request->Saturday != ""){
                $hour->Saturday = $request->Saturday; 
            }
            if($request->Sunday != ""){
                $hour->Sunday = $request->Sunday; 
            }
            $hour->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function AddTable(Request $request){
        try {
            $table = new Tables();
            $table->id_restaurant = $request->id_restaurant;
            $table->table_number = $request->table_number;
            $table->is_taken = "0";
            $table->saveOrFail();    
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil Ditambah',
                'dataHours' => [$table]
            ];  
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Ditambah',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getTables(Request $request){
        try {
            $data = DB::table('tables')
            ->select('tables.*')
            ->where('tables.id_restaurant', $request->id_restaurant)
            ->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataTables' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateTable(Request $request){
        try{
            $table=Tables::where('id','=',$request->id_table)->first();
            if($request->table_number != ""){
                $table->table_number = $request->table_number; 
            }
            $table->saveOrFail();
    
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'update berhasil',
            ];     
        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getOrderStatistic(Request $request){
        try {
            $order =Orders::select(DB::raw('count(id) as `data`'),DB::raw("DATE_FORMAT(created_at, '%Y-%m') date"))
            ->where('id_restaurant', $request->id_restaurant)
            ->groupBy('date')
            ->orderBy('date')->get();
            if(!$order->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'orderStatistic' => $order
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getCallingStatistic(Request $request){
        try {
            $data = DB::table('callings')
            ->join('calling_types', 'callings.calling_type', '=', 'calling_types.id')
            ->selectRaw('calling_types.name as type, count(callings.id) as data')
            ->where('callings.id_restaurant', $request->id_restaurant)
            ->groupBy('type')
            ->orderBy('type', 'ASC')
            ->get();
            // $order =Callings::select(DB::raw('count(id) as `data`'),DB::raw("calling_type date"))
            // ->where('id_restaurant', $request->id_restaurant)
            // ->groupBy('date')
            // ->orderBy('date')->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'callingStatistic' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getCheckinStatistic(Request $request){
        try {
            $order =Checkins::select(DB::raw('count(id) as `data`'),DB::raw("DATE_FORMAT(created_at, '%Y-%m') date"))
            ->where('id_restaurant', $request->id_restaurant)
            ->groupBy('date')
            ->orderBy('date')->get();
            if(!$order->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'orderStatistic' => $order
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    
    public function getItemOrderStatistic(Request $request){
        try {
            $data = DB::table('item_orders')
            ->join('menus', 'item_orders.id_menu', '=', 'menus.id')
            ->join('orders', 'item_orders.id_order', '=', 'orders.id')
            ->selectRaw('menus.name as menu, sum(item_orders.quantity) as data')
            ->where('orders.id_restaurant', $request->id_restaurant)
            ->groupBy('menu')
            ->orderBy('menu', 'ASC')
            ->get();
            // $order =Callings::select(DB::raw('count(id) as `data`'),DB::raw("calling_type date"))
            // ->where('id_restaurant', $request->id_restaurant)
            // ->groupBy('date')
            // ->orderBy('date')->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'callingStatistic' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function AddFeedback(Request $request){
        try {
            if($request->type == "1"){
                $feedback = new Feedbacks();
                $feedback->id_restaurant = $request->id_restaurant;
                $feedback->question = $request->question;
                $feedback->type = $request->type;
                $feedback->saveOrFail();    
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Ditambah',
                    'dataFeedback' => [$feedback]
                ];  
            }else if($request->type == "2"){
                $feedback = new Feedbacks();
                $feedback->id_restaurant = $request->id_restaurant;
                $feedback->question = $request->question;
                $feedback->type = $request->type;
                $feedback->saveOrFail();    
        
                $feedbackOption = new FeedbackOptionals();
                $feedbackOption->id_feedback = $feedback->id;
                $feedbackOption->option_A = $request->option_A;
                $feedbackOption->option_B = $request->option_B;
                $feedbackOption->option_C = $request->option_C;
                $feedbackOption->option_D = $request->option_D;
                $feedbackOption->saveOrFail();  
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Ditambah',
                    'dataFeedback' => [$feedback],
                    'dataFeedbackOption' => [$feedbackOption]
                ];  
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Ditambah',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Ditambah',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getFeedbacksRestaurant(Request $request){
        try {
            $data = DB::table('feedbacks')
            ->leftJoin('feedback_optionals', 'feedbacks.id', '=', 'feedback_optionals.id_feedback')
            ->join('feedback_types', 'feedbacks.type', '=', 'feedback_types.id')
            ->select('feedbacks.*','feedback_types.description','feedback_optionals.option_A','feedback_optionals.option_B','feedback_optionals.option_C','feedback_optionals.option_D')
            ->where('feedbacks.id_restaurant', $request->id_restaurant)
            ->orderBy('feedbacks.id', 'ASC')
            ->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataFeedbacks' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateFeedbacks(Request $request){
        try{
            $feedback=Feedbacks::where('id','=',$request->id_feedback)->first();
            if($feedback->type == "1"){
                $feedback->question = $request->question;
                $feedback->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'update berhasil',
                ];     
            }else if($feedback->type == "2"){
                $feedback->question = $request->question;
                $feedback->saveOrFail();

                $feedbackOption=FeedbackOptionals::where('id_feedback','=',$request->id_feedback)->first();
                $feedbackOption->option_A = $request->option_A;
                $feedbackOption->option_B = $request->option_B;
                $feedbackOption->option_C = $request->option_C;
                $feedbackOption->option_D = $request->option_D;
                $feedbackOption->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'update berhasil',
                ];     
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal update',
                ];   
            }
        }catch (Exception $e){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal update',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getFeedbackAnswers(Request $request){
        try {
            $data = DB::table('feedback_answers')
            ->join('feedbacks', 'feedback_answers.id_feedback', '=', 'feedbacks.id')
            ->join('users', 'feedback_answers.id_user', '=', 'users.id')
            ->leftJoin('feedback_optionals', 'feedback_answers.id_feedback', '=', 'feedback_optionals.id_feedback')
            ->select('feedback_answers.id','feedbacks.question','feedbacks.type','users.name','feedback_answers.answer','feedback_optionals.option_A','feedback_optionals.option_B','feedback_optionals.option_C','feedback_optionals.option_D')
            ->where('feedbacks.id_restaurant', $request->id_restaurant)
            ->orderBy('feedback_answers.id', 'ASC')
            ->get();
            if(!$data->isEmpty()){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data ditemukan',
                    'dataAnswers' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data tidak ditemukan',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }





    //Method for Restaurant Admin
    //END
    

}